#include <stdio.h>


float area(float ancho, float largo)
{
    return ancho*largo;
}


int main(void)
{
    const float PIES_to_ACRE = 43560.0;

    float ancho = 0.0;
    float largo = 0.0;
    float resultado = 0.0;


    printf("Ingrese el ancho del campo: \n");
    scanf("%f", &ancho);

    printf("Ingrese el largo del campo: \n");
    scanf("%f", &largo);


    resultado = area(ancho, largo);
    resultado /= PIES_to_ACRE;


    printf("El área del campo es de  %f acre.\n", resultado);


    return 0;
}

