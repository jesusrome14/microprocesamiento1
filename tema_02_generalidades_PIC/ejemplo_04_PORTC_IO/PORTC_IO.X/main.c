#include <xc.h>

#pragma config FOSC = HS 
#pragma config WDTE = OFF 
#pragma config PWRTE = OFF 
#pragma config MCLRE = ON
#pragma config CP = OFF 
#pragma config CPD = OFF 
#pragma config BOREN = OFF 
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF 
#pragma config DEBUG = OFF 

#define _XTAL_FREQ 20000000


int main(void)
{
    PORTC = 0x00;
    TRISC = 0xC0;
    
    while(1)
    {
        if(RC6 == 0)
        {
            __delay_ms(100);

            if(RC6 == 0)
            {
              PORTC = 0xC0;
              __delay_ms(100);
            }
        }
        
        if(RC7 == 0)
        {
            __delay_ms(100);

            if(RC7 == 0)
            {
              PORTC = 0x3F;
              __delay_ms(100);
            }
        }
    }
    
    return 0;
}
