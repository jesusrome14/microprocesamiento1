#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <xc.h>
#include "lcd.h"

#pragma config FOSC = HS 
#pragma config WDTE = OFF 
#pragma config PWRTE = OFF 
#pragma config MCLRE = ON
#pragma config CP = OFF 
#pragma config CPD = OFF 
#pragma config BOREN = OFF 
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF 
#pragma config DEBUG = OFF 

#define _XTAL_FREQ 20000000

unsigned char i = 0;
unsigned char c = 0;
char text[10] = " Micros 1 ";

void Inicializar_ADC()
{
    ADCON0 = 0x81; 
    ADCON1 = 0X00; 
}

unsigned int Leer_ADC(unsigned char canal)
{
    if(canal > 13)
        return 0;
    
    ADCON0 &= 0xC5;     
    ADCON0 |= canal<<2; 
    __delay_ms(2);      
    GO_nDONE = 1;       
    while(GO_nDONE);    
    
    return ((ADRESH<<8) + ADRESL); 
}


int main ( void )
{
    unsigned int dato_adc = 0;
    float dato_lcd = 0.0;
    char dato_char[30];
    
    PORTA  = 0x00; 
    TRISA  = 0x01;
    ANSEL  = 0x01;

    LCD lcd = { &PORTD, 2, 3, 4, 5, 6, 7 }; // PORT, RS, EN, D4, D5, D6, D7

    Inicializar_ADC();
    
    LCD_Init(lcd);
    LCD_Clear();
    
    while(1)
    {
        dato_adc = Leer_ADC(0);
        
        dato_lcd = 5.0*dato_adc/1024.0;
        sprintf(dato_char, "%.4f", dato_lcd);        
        
        LCD_Set_Cursor(0, 0);
        LCD_putrs("Voltaje:");
        
        LCD_Set_Cursor(1,0);
        LCD_putrs(dato_char);
        LCD_putrs(" V ");
        
	 	__delay_ms(10);
	}

    return 0;
}
