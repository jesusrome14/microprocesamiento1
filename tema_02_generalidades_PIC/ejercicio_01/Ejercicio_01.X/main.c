#include <xc.h>

#pragma config FOSC = HS 
#pragma config WDTE = OFF 
#pragma config PWRTE = OFF 
#pragma config MCLRE = ON
#pragma config CP = OFF 
#pragma config CPD = OFF 
#pragma config BOREN = OFF 
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF 
#pragma config DEBUG = OFF 

#define _XTAL_FREQ 20000000


int main(void)
{
    TRISD0 = 0; //Pone el RB0 como salida.
    
    while(1)
    {
        RD0 = 1; //Encender el LED.
        __delay_ms(1000); //Retardo de un segundo.
        
        RD0 = 0; //Apagar el LED.
        __delay_ms(1000);
    }  

    return 0;
}
